package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class CalculatorActivity : AppCompatActivity(), View.OnClickListener {
    private var firstVariable:Double = 0.0
    private var secondVariable:Double = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }
    private fun init(){
        zeroButton.setOnClickListener(this)
        oneButton.setOnClickListener(this)
        twoButton.setOnClickListener(this)
        threeButton.setOnClickListener(this)
        fourButton.setOnClickListener(this)
        fiveButton.setOnClickListener(this)
        sixButton.setOnClickListener(this)
        sevenButton.setOnClickListener(this)
        eightButton.setOnClickListener(this)
        nineButton.setOnClickListener(this)

        backspaseButton.setOnLongClickListener {
            result.text=""
            return@setOnLongClickListener true
        }

        dotButton.setOnClickListener {
            if(result.text.isNotEmpty() && "." !in result.text.toString()){
                result.text = result.text.toString() + "."
            }
        }
    }


    fun assembly(view: View){
        var value = result.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "+"
            result.text = ""
        }

    }

    fun subtraction(view: View){
        var value = result.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "-"
            result.text = ""
        }

    }

    fun multiplication(view: View){
        var value = result.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = "X"
            result.text = ""
        }

    }

    fun equal(view: View){
        val value = result.text.toString()
        if(value.isNotEmpty() && operation.isNotEmpty()) {
            secondVariable = value.toDouble()
            var finalResult:Double = 0.0
            if (operation == ":") {
                finalResult = firstVariable / secondVariable
            }
            if(operation == "X") {
                finalResult = firstVariable * secondVariable
            }
            if(operation == "+"){
                finalResult = firstVariable + secondVariable
            }
            if(operation == "-"){
                finalResult = firstVariable - secondVariable
            }

            result.text = finalResult.toString()

            operation = ""
            firstVariable = 0.0
            secondVariable = 0.0
        }
    }

    fun delete(view: View){
        val value = result.text.toString()
        if(value.isNotEmpty())
            result.text = value.substring(0, value.length - 1)
    }
    fun divide(view: View){
        var value = result.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = ":"
            result.text = ""
        }
    }

    override fun onClick(v: View?) {
        val button = v as Button
        result.text=result.text.toString() + button.text.toString()
    }
}